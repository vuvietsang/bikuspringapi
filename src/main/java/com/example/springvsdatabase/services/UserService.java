package com.example.springvsdatabase.services;

import com.example.springvsdatabase.dto.UpdateUserDto;
import com.example.springvsdatabase.model.User;

import org.springframework.data.domain.Page;

import java.util.List;

public interface UserService {
    User findUserByUserName(String username);

    boolean register(User user);

    Page<User> getAllUser(int pageNum, int pageSize);
    List<User> getAllUser2();
    User getUserById(int userId);

    boolean activateUser(int userId, boolean status);

    boolean updateUser(UpdateUserDto user, int userId);
    boolean deleteUserById(int userId);
}
