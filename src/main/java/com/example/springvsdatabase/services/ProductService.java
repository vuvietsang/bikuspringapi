package com.example.springvsdatabase.services;

import com.example.springvsdatabase.model.Products;

import org.springframework.data.domain.Page;

public interface ProductService {
    Products getProduct(int idPro);

    boolean deletePro(int idPro);

    boolean updatePro(Products product, int idPro);

    boolean addPro(Products product);

    Page<Products> getAllProducts(int pageNum, int pageSize);

    Page<Products> findProductByProductName(String productName, int pageNum, int pageSize);

    Page<Products> findProductByMaker(String maker, int pageNum, int pageSize);

    Page<Products> findProductByPrice(double min, double max, int pageNum, int pageSize);

}
