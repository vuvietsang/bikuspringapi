package com.example.springvsdatabase.services;

import com.example.springvsdatabase.dto.OrderDetailsDto;
import com.example.springvsdatabase.model.OrderDetails;
import com.example.springvsdatabase.model.Orders;

import org.springframework.data.domain.Page;

import java.util.List;

public interface OrdersService {
    Orders getOrder(int orderId);

    boolean deleteOrder(int orderId);

    boolean updateOrder(Orders order, int orderId);

    boolean checkout(OrderDetailsDto[] orderDetails);

    boolean confirmOrder(int orderId);

    Page<Orders> getAllOrders(int pageNum, int pageSize);

   Page<Orders> getOrderByUserId(int pageNum, int pageSize, int userId);

   List<Orders> getOrdersByEmail(String email);
}
