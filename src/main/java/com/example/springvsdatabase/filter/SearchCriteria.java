package com.example.springvsdatabase.filter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class SearchCriteria {
        private String key;
        private String operation;
        private Object value;
}
