package com.example.springvsdatabase.enumcode;

public enum ErrorCode {
    SIGN_UP_FAILED, ADD_PRODUCT_TO_CART, GET_USER, GET_ALL_PRODUCT, LOGIN_FAIL, ACTIVATE_USER, UPDATE_USER, DELETE_PRODUCT,
    ADD_PRODUCT, UPDATE_PRODUCT, GET_PRODUCT, GET_PRODUCT_BY_NAME, CHECK_OUT, DELETE_ORDER, GET_ALL_ORDERS,
    GET_ALL_USERS, CONFIRM_ORDER,DELETE_USER_FAIL,GET_ORDER_BY_ID_FAIL,GET_ORDER_BY_EMAIL_FAIL
}
