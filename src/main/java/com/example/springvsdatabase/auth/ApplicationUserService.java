package com.example.springvsdatabase.auth;

import com.example.springvsdatabase.model.User;
import com.example.springvsdatabase.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Service
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ApplicationUserService implements UserDetailsService {
    @Autowired
    private UserRepository repo;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = repo.findUserByUserName(userName);
        if (user == null) {
            throw new UsernameNotFoundException("USER_NAME_NOT_FOUND!");
        }
        return new UserDetail(user);
    }

}
