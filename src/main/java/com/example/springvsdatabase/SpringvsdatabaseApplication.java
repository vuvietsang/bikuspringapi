package com.example.springvsdatabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringvsdatabaseApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringvsdatabaseApplication.class, args);
	}

}
