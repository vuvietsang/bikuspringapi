package com.example.springvsdatabase.repository;

import com.example.springvsdatabase.model.Orders;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface OrdersRepository extends JpaRepository<Orders, Integer>, JpaSpecificationExecutor<Orders> {
    Page<Orders> findByUserId(Pageable pageable, int userId);

    List<Orders> getOrdersByUserEmail(String email);
}
