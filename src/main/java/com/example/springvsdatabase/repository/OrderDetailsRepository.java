package com.example.springvsdatabase.repository;

import com.example.springvsdatabase.model.OrderDetails;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDetailsRepository extends JpaRepository<OrderDetails, Object> {

    @Query(value = "SELECT  * from order_details od " +
            "join orders o " +
            "on o.id  = od.order_id " +
            "WHERE o.user_id  = ?1" , nativeQuery = true)
    List<OrderDetails> getAllOrderDetail (Long userId);

}
