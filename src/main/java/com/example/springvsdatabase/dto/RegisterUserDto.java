package com.example.springvsdatabase.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RegisterUserDto {
    private String name;
    private String userName;
    private String role;
    private String email;
    private String password;
}
