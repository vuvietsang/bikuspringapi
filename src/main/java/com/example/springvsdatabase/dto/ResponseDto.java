package com.example.springvsdatabase.dto;

import com.example.springvsdatabase.enumcode.ErrorCode;
import com.example.springvsdatabase.enumcode.SuccessCode;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseDto {
    private SuccessCode successCode;
    private Object data;
    private ErrorCode errorCode;
}
