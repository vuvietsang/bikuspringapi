package com.example.springvsdatabase.serviceImp;

import com.example.springvsdatabase.dto.UpdateUserDto;
import com.example.springvsdatabase.model.Roles;
import com.example.springvsdatabase.model.User;
import com.example.springvsdatabase.repository.UserRepository;
import com.example.springvsdatabase.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImp implements UserService {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserRepository repository;

    public UserServiceImp(UserRepository repository) {
        this.repository = repository;
    }

    public UserServiceImp() {
    }

    @Override
    public User findUserByUserName(String username) {
        return repository.findUserByUserName(username);
    }

    @Override
    public boolean register(User user) {
       try {
           this.repository.save(user);
           return true;
       }
       catch (Exception e){
           return false;
       }

    }

    @Override
    public Page<User> getAllUser(int pageNum, int pageSize) {
        Pageable pageable = PageRequest.of(pageNum, pageSize);
        Page<User> pageUser = this.repository.findAll(pageable);
        return pageUser;
    }
    @Override
    public List<User> getAllUser2() {
        List<User> pageUsers = this.repository.findAll();
        return pageUsers;
    }

    @Override
    public User getUserById(int userId) {
        return this.repository.findById(userId).get();
    }

    @Override
    public boolean activateUser(int userId, boolean status) {
        try {
            User user = this.repository.findById(userId).get();
            user.setStatus(status);
            this.repository.save(user);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean updateUser(UpdateUserDto user, int userId) {
        try {
            User userRepo = this.repository.findById(userId).get();
            userRepo.setEmail(user.getEmail());
            userRepo.setName(user.getName());
            userRepo.setPassword(passwordEncoder.encode(user.getPassword()));
            if (user.getRole().equals("ADMIN")) {
                userRepo.setRole(Roles.builder().id(1).build());
            } else if (user.getRole().equals("USER")) {
                userRepo.setRole(Roles.builder().id(2).build());
            }
            userRepo.setUserName(user.getUserName());
            userRepo.setCreateDate(userRepo.getCreateDate());
            this.repository.save(userRepo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteUserById(int userId) {
        try {
            this.repository.deleteById(userId);
            return true;
        }
        catch (Exception e){
            e.printStackTrace();
            return  false;
        }
    }
}
