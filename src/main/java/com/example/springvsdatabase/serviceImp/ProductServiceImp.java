package com.example.springvsdatabase.serviceImp;

import com.example.springvsdatabase.model.Products;
import com.example.springvsdatabase.repository.ProductRepository;
import com.example.springvsdatabase.services.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ProductServiceImp implements ProductService {
    private ProductRepository repository;

    @Override
    public Products getProduct(int idPro) {
        return this.repository.findById(idPro).get();
    }

    @Override
    public boolean deletePro(int idPro) {
        try {
            this.repository.deleteById(idPro);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean updatePro(Products product, int idPro) {
        try {
            Products products = this.repository.findById(idPro).get();
            products.setName(product.getName());
            products.setMaker(product.getMaker());
            products.setCategory(product.getCategory());
            products.setQuantity(product.getQuantity());
            products.setPrice(product.getPrice());
            products.setMinQuantity(product.getMinQuantity());
            products.setImgURL(product.getImgURL());
            products.setCreateDate(product.getCreateDate());
            products.setDescription((product.getDescription()));
            this.repository.save(products);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Page<Products> getAllProducts(int pageNum, int pageSize) {
        Pageable pageable = PageRequest.of(pageNum, pageSize);
        Page<Products> pageProduct = this.repository.findAll(pageable);
        return pageProduct;
    }

    @Override
    public boolean addPro(Products product) {
        try {
            this.repository.save(product);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public Page<Products> findProductByProductName(String productName, int pageNum, int pageSize) {
        Pageable pageable = PageRequest.of(pageNum, pageSize);
        Page<Products> pageProduct = this.repository.findProductsByName(pageable, productName);
        return pageProduct;
    }

    @Override
    public Page<Products> findProductByMaker(String maker, int pageNum, int pageSize) {
        Pageable pageable = PageRequest.of(pageNum, pageSize);
        Page<Products> pageProduct = this.repository.findProductsByMaker(pageable, maker);
        return pageProduct;
    }

    @Override
    public Page<Products> findProductByPrice(double min, double max, int pageNum, int pageSize) {
        Pageable pageable = PageRequest.of(pageNum, pageSize);
        Page<Products> pageProduct = this.repository.findProductsByPriceBetween(pageable, min, max);
        return pageProduct;
    }

}
