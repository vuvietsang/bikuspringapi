package com.example.springvsdatabase.controller;

import com.example.springvsdatabase.dto.OrderDetailsDto;
import com.example.springvsdatabase.dto.ResponseDto;
import com.example.springvsdatabase.enumcode.ErrorCode;
import com.example.springvsdatabase.enumcode.SuccessCode;
import com.example.springvsdatabase.model.OrderDetails;
import com.example.springvsdatabase.model.Orders;
import com.example.springvsdatabase.services.OrdersService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("order")
@CrossOrigin(origins = "*")
public class OrderController {
    @Autowired
    private OrdersService service;

    @PostMapping("/checkout")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<ResponseDto> checkout(@RequestBody OrderDetailsDto[] orderDetails) {
        ResponseDto response = new ResponseDto();
        try {
            response.setData(this.service.checkout(orderDetails));
            response.setSuccessCode(SuccessCode.CHECK_OUT);
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            response.setErrorCode(ErrorCode.CHECK_OUT);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
    }

    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ResponseDto> deleteOrder(@PathVariable("id") int orderId) {
        ResponseDto response = new ResponseDto();
        try {
            response.setData(this.service.deleteOrder(orderId));
            response.setSuccessCode(SuccessCode.DELETE_ORDER);
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            response.setErrorCode(ErrorCode.DELETE_ORDER);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
    }

    @GetMapping("/orders")
    @PreAuthorize("hasAnyRole('ADMIN','USER','SALER')")
    public ResponseEntity<ResponseDto> getAllOrders(@RequestParam int pageNum, @RequestParam int pageSize) {
        ResponseDto response = new ResponseDto();
        try {
            response.setData(this.service.getAllOrders(pageNum, pageSize));
            response.setSuccessCode(SuccessCode.GET_ALL_ORDERS);
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            response.setErrorCode(ErrorCode.GET_ALL_ORDERS);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
    }

    @PutMapping("/confirm/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','SALER')")
    public ResponseEntity<ResponseDto> confirmOrder(@PathVariable("id") int orderId) {
        ResponseDto response = new ResponseDto();
        try {
            response.setData(this.service.confirmOrder(orderId));
            response.setSuccessCode(SuccessCode.CONFIRM_ORDER);
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            response.setErrorCode(ErrorCode.CONFIRM_ORDER);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
    }
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','USER','SALER')")
    public ResponseEntity<ResponseDto> getOrderById(@PathVariable("id") int orderId){
        ResponseDto response = new ResponseDto();
        try {
            response.setData(service.getOrder(orderId));
            response.setSuccessCode(SuccessCode.GET_ORDER_BY_ID_SUCCESSFULLY);
            return ResponseEntity.ok().body(response);
        }
        catch (Exception e){
            e.printStackTrace();
            response.setErrorCode(ErrorCode.GET_ORDER_BY_ID_FAIL);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
    }
    @GetMapping("/email/{email}")
    @PreAuthorize("hasAnyRole('ADMIN','SALER')")
    public ResponseEntity<ResponseDto> getOrderByEmail(@PathVariable("email") String email){
        System.out.println("HIHIHIHIHI"+email);
        ResponseDto response = new ResponseDto();
        try {
            response.setData(service.getOrdersByEmail(email));
            response.setSuccessCode(SuccessCode.GET_ORDER_BY_ID_SUCCESSFULLY);
            return ResponseEntity.ok().body(response);
        }
        catch (Exception e){
            e.printStackTrace();
            response.setErrorCode(ErrorCode.GET_ORDER_BY_ID_FAIL);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
    }
    @GetMapping("/myorder/{id}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<ResponseDto> getOrderByUserId(@PathVariable("id") int userId,@RequestParam int pageNum, @RequestParam int pageSize){
        ResponseDto response = new ResponseDto();
        try {
            response.setData(service.getOrderByUserId(pageNum,pageSize,userId));
            response.setSuccessCode(SuccessCode.GET_ORDER_BY_ID_SUCCESSFULLY);
            return ResponseEntity.ok().body(response);
        }
        catch (Exception e){
            e.printStackTrace();
            response.setErrorCode(ErrorCode.GET_ORDER_BY_ID_FAIL);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
    }
}
