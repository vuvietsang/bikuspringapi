package com.example.springvsdatabase.controller;

import javax.annotation.security.PermitAll;

import com.example.springvsdatabase.dto.ResponseDto;
import com.example.springvsdatabase.enumcode.ErrorCode;
import com.example.springvsdatabase.enumcode.SuccessCode;
import com.example.springvsdatabase.model.Products;
import com.example.springvsdatabase.services.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("product")
public class ProductController {
    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/products")
    @PermitAll
    public ResponseEntity<ResponseDto> getProducts(@RequestParam int pageNum, @RequestParam int pageSize) {
        ResponseDto responseDto = new ResponseDto();
        responseDto.setData(this.productService.getAllProducts(pageNum, pageSize));
        responseDto.setSuccessCode(SuccessCode.GET_ALL_PRODUCT);
        return ResponseEntity.ok().body(responseDto);
    }

    @DeleteMapping("/products/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ResponseDto> deleteProduct(@PathVariable("id") int productId) {
        ResponseDto responseDto = new ResponseDto();
        if (this.productService.deletePro(productId)) {
            responseDto.setSuccessCode(SuccessCode.DELETE_PRODUCT);
            responseDto.setData(this.productService.deletePro(productId));
        } else {
            responseDto.setErrorCode(ErrorCode.DELETE_PRODUCT);
        }
        return ResponseEntity.ok().body(responseDto);
    }

    @PostMapping("/products/add")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ResponseDto> addProduct(@RequestBody Products product) {
        ResponseDto responseDto = new ResponseDto();
        if (this.productService.addPro(product)) {
            responseDto.setSuccessCode(SuccessCode.ADD_PRODUCT);
            responseDto.setData(this.productService.addPro(product));
        } else {
            responseDto.setErrorCode(ErrorCode.ADD_PRODUCT);
        }
        return ResponseEntity.ok().body(responseDto);
    }

    @PutMapping("/products/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<ResponseDto> updateProduct(@PathVariable("id") int productId, @RequestBody Products product) {
        ResponseDto responseDto = new ResponseDto();
        if (this.productService.updatePro(product, productId)) {
            responseDto.setSuccessCode(SuccessCode.UPDATE_PRODUCT);
            responseDto.setData(this.productService.updatePro(product, productId));
        } else {
            responseDto.setErrorCode(ErrorCode.UPDATE_PRODUCT);
        }
        return ResponseEntity.ok().body(responseDto);
    }

    @GetMapping("/products/{id}")
    @PermitAll
    public ResponseEntity<ResponseDto> getProductById(@PathVariable("id") int idPro) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setSuccessCode(SuccessCode.GET_PRODUCT);
            responseDto.setData(this.productService.getProduct(idPro));
            return ResponseEntity.ok().body(responseDto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

    }

    @GetMapping("/products/name/{name}")
    public ResponseEntity<ResponseDto> findProductsByProductName(@PathVariable("name") String productName,
            @RequestParam int pageNum, @RequestParam int pageSize) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setSuccessCode(SuccessCode.GET_PRODUCT_BY_NAME);
            responseDto.setData(this.productService.findProductByProductName(productName, pageNum, pageSize));
            return ResponseEntity.ok().body(responseDto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
    @GetMapping("/products/maker/{makername}")
    @PermitAll
    public ResponseEntity<ResponseDto> findProductsByMaker(@PathVariable("makername") String maker,
            @RequestParam int pageNum, @RequestParam int pageSize) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setSuccessCode(SuccessCode.GET_PRODUCT_BY_MAKER);
            responseDto.setData(this.productService.findProductByMaker(maker, pageNum, pageSize));
            return ResponseEntity.ok().body(responseDto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

    }

    @GetMapping("/products/price/{min}/{max}")
    @PermitAll
    public ResponseEntity<ResponseDto> findProductsByPrice(@PathVariable("min") double min,
            @PathVariable("max") double max, @RequestParam int pageNum, @RequestParam int pageSize) {
        ResponseDto responseDto = new ResponseDto();
        try {
            responseDto.setSuccessCode(SuccessCode.GET_PRODUCT_BY_PRICE);
            responseDto.setData(this.productService.findProductByPrice(min, max, pageNum, pageSize));
            return ResponseEntity.ok().body(responseDto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

    }
}
