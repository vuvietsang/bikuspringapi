package com.example.springvsdatabase.model;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;

@Entity

@Builder
public class OrderDetails {

    @EmbeddedId
    private OrderDetailsId id = new OrderDetailsId();

    @JsonIgnore
    @ManyToOne
    @MapsId("orderId")
    private Orders order;

    @ManyToOne
    @MapsId("productId")
    private Products product;

    @Transient
    private int productId;

    private int quantity;
    private double price;

    public OrderDetails() {
    }

    public OrderDetails(OrderDetailsId id, Orders order, Products product, int productId, int quantity, double price) {
        this.id = new OrderDetailsId(order.getId(), product.getId());
        this.order = order;
        this.product = product;
        this.productId = productId;
        this.quantity = quantity;
        this.price = price;
    }

    @Embeddable
    public static class OrderDetailsId implements Serializable {
        private int productId;
        private int orderId;

        public OrderDetailsId(int orderId, int productId) {
            super();
            this.productId = productId;
            this.orderId = orderId;
        }

        public OrderDetailsId() {
        }

        public int getUserId() {
            return productId;
        }

        public void setUserId(int userId) {
            this.productId = userId;
        }

        public int getOrderId() {
            return orderId;
        }

        public void setOrderId(int orderId) {
            this.orderId = orderId;
        }

    }

    public OrderDetailsId getId() {
        return id;
    }

    public void setId(OrderDetailsId id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Products getProduct() {
        return product;
    }

    public void setProduct(Products product) {
        this.product = product;
    }

    public Orders getOrder() {
        return order;
    }

    public void setOrder(Orders order) {
        this.order = order;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

}
