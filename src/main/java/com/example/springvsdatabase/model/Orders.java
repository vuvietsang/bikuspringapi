package com.example.springvsdatabase.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Orders {
    @Id
    @GeneratedValue
    private int id;
    private boolean status;
    private LocalDate createDate;
    @JsonIgnore
    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "order")
    private List<OrderDetails> orderedProduct = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<OrderDetails> getOrderedProduct() {
        return orderedProduct;
    }

    public void setOrderedProduct(List<OrderDetails> orderedProduct) {
        this.orderedProduct = orderedProduct;
    }

    public Orders(int id, boolean status, LocalDate createDate, User user, List<OrderDetails> orderedProduct) {
        this.id = id;
        this.status = status;
        this.createDate = createDate;
        this.user = user;
        this.orderedProduct = orderedProduct;
    }

    public Orders(boolean status, LocalDate createDate, User user) {
        this.status = status;
        this.createDate = createDate;
        this.user = user;
    }

    public Orders() {
    }

}
