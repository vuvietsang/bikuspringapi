package com.example.springvsdatabase.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Products {
    @Id
    @GeneratedValue
    private int id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "maker", nullable = false)
    private String maker;
    @Column(name = "category", nullable = false)
    private int category;
    @Column(name = "quantity", nullable = false)
    private int quantity;
    @Column(name = "price", nullable = false)
    private double price;
    @Column(name = "minQuantity", nullable = false)
    private int minQuantity;
    @Column(name = "imgURL", nullable = false)
    private String imgURL;
    private LocalDate createDate;
    private String description;
    @JsonIgnore
    @OneToMany(mappedBy = "product")
    private List<OrderDetails> orderedProduct = new ArrayList<>();

    public Products(int id, String name, String maker, int category, int quantity, double price, int minQuantity,
            String imgURL, LocalDate createDate, String description, List<OrderDetails> orderedProduct) {
        this.id = id;
        this.name = name;
        this.maker = maker;
        this.category = category;
        this.quantity = quantity;
        this.price = price;
        this.minQuantity = minQuantity;
        this.imgURL = imgURL;
        this.createDate = createDate;
        this.description = description;
        this.orderedProduct = orderedProduct;
    }

    public Products() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getMinQuantity() {
        return minQuantity;
    }

    public void setMinQuantity(int minQuantity) {
        this.minQuantity = minQuantity;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<OrderDetails> getOrderedProduct() {
        return orderedProduct;
    }

    public void setOrderedProduct(List<OrderDetails> orderedProduct) {
        this.orderedProduct = orderedProduct;
    }

}
