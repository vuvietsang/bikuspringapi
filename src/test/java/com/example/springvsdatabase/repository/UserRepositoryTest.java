package com.example.springvsdatabase.repository;

import com.example.springvsdatabase.model.Roles;
import com.example.springvsdatabase.model.User;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
@DataJpaTest
class UserRepositoryTest {
    @Autowired
    private UserRepository underTest;

    @Test
    void testIfFoundUser(){
        User user = User.builder().id(1).userName("sang").name("sang")
                .email("vuvietsang10a9@gmail.com")
                .role(Roles.builder().roleName("ADMIN").id(1).build()).status(true).createDate(LocalDate.now()).password("1234566789").build();
        underTest.save(user);
        User expected =  underTest.findUserByUserName(user.getUserName());
        AssertionsForClassTypes.assertThat(expected).isNotNull();
    }
}